package nessiesson.nolight.mixins;

import io.netty.channel.Channel;
import io.netty.channel.ChannelException;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.NettyPacketDecoder;
import net.minecraft.network.NettyPacketEncoder;
import net.minecraft.network.NettyVarint21FrameDecoder;
import net.minecraft.network.NettyVarint21FrameEncoder;
import net.minecraft.network.NetworkManager;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(targets="net.minecraft.network.NetworkManager$5")
public class MixinNetworkManager$5 {

	@Shadow(aliases={"val$networkmanager"}) private @Final NetworkManager field_179248_a;

	@Overwrite
	protected void initChannel(Channel p_initChannel_1_) throws Exception {
		try
		{
			p_initChannel_1_.config().setOption(ChannelOption.TCP_NODELAY, Boolean.valueOf(true));
		}
		catch (ChannelException var3)
		{
			;
		}

		p_initChannel_1_.pipeline().addLast("timeout", new ReadTimeoutHandler(0))
				.addLast("splitter", new NettyVarint21FrameDecoder()).addLast("decoder", new NettyPacketDecoder(EnumPacketDirection.CLIENTBOUND))
				.addLast("prepender", new NettyVarint21FrameEncoder()).addLast("encoder", new NettyPacketEncoder(EnumPacketDirection.SERVERBOUND))
				.addLast("packet_handler", field_179248_a);
	}
}